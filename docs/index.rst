.. include:: ../README.rst

Documentation Contents
----------------------

.. toctree::
   :maxdepth: 2

   api
   contributing



Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
