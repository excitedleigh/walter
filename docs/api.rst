API
===

Config
------

.. autoclass:: walter.config.Config
    :members:

Sources
-------

Built-In
::::::::

.. autoclass:: walter.sources.EnvironmentSource

.. autoclass:: walter.sources.IniFileSource

Creating Your Own
:::::::::::::::::

.. autoclass:: walter.sources.Source
    :members:

.. autoclass:: walter.sources.FileSource
    :members:
